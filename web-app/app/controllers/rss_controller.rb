require "rss"

class RssController < ApplicationController
  skip_before_action :authenticate_user!, only: [:rss, :atom]

  def rss
    render :plain => maker("2.0")
  end

  def atom
    render :plain => maker("atom")
  end

  def maker(xml)
    xml_link = "http://my-city.com/" + xml
    messages = Message.all

    rss = RSS::Maker.make(xml) do |maker|
      maker.channel.author = "my-city"
      maker.channel.updated = Time.now.to_s
      xml == "2.0" ? maker.channel.description = xml_link : maker.channel.about = xml_link
      maker.channel.link = xml_link
      maker.channel.title = "My City Feed"

      messages.each do |message|
        maker.items.new_item do |item|
          item.link = url_for(message)
          item.title = message.body.truncate(50)
          item.updated = message.created_at.to_s
        end
      end
    end

    return rss
  end
end

