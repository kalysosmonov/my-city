class UsersController < ApplicationController
  before_action :role_required # проверяет роль юзера
  def new
  	@user = User.new
  end

  def edit
  	@user = User.find(params[:id])
  end

  def index
  	@users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def create
  	@user = User.new(user_params)
  	if @user.save
  		redirect_to root_path
  		flash[:success] = "Пользователь успешно зарегистрирован."
  	else
  		render "new"
  		flash[:danger] = "Ошибка."
  	end
  end

  def update
  	@user = User.find(params[:id])
  	if @user.update(user_params)
  		redirect_to root_path
  		flash[:success] = "Пользователь успешно обновлен."
  	else
  		render "edit"
  		flash[:danger] = "Ошибка."
  	end
  end

  def destroy
  	@user = User.find(params[:id])
  	@user.destroy

  	redirect_to root_path
  end

  private

  def user_params
  	params.require(:user).permit(:name, :email, :password, :role_id)
  end
end
